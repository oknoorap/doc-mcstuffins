# Doc McStuffins

In this repository doc is stands for **Document** not a doctor. A PDF site content generator.

### Install
Currently installation only support for Ubuntu.  
Go to your ssh terminal and paste this line of command:
####
```bash
curl https://bitbucket.org/!api/2.0/snippets/oknoorap/nebGbz/files/install.sh | bash
```

### Contributing
Please report issue to me if you found any problems.

### License
MIT - copyright (c) 2018 Ribhararnus Pracutian